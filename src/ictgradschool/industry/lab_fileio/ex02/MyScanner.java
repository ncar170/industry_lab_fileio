package ictgradschool.industry.lab_fileio.ex02;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        File scannerFile = new File("input2.txt");
        File fileExample = new File ("fileExample.txt");

        try(Scanner scanner = new Scanner(scannerFile)){
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }

        } catch (IOException e) {

        }

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
