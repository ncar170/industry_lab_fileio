package ictgradschool.industry.lab_fileio.ex02;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyReader {

    public void start() {

        File readFile = new File("input2.txt");

        try (BufferedReader reader = new BufferedReader(new FileReader(readFile))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        }   catch (IOException e) {
            System.out.println("IO Error");
        }


        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
