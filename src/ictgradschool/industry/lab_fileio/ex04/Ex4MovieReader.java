package ictgradschool.industry.lab_fileio.ex04;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab_fileio.ex03.Movie;
import ictgradschool.industry.lab_fileio.ex03.MovieReader;

import java.io.File;
import java.util.Scanner;
import java.io.IOException;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    protected Movie[] loadMovies(String fileName) {

        Movie movies [] = new Movie[19];

        File myFile = new File(fileName);

        try (Scanner scanner = new Scanner(myFile)) {

            scanner.useDelimiter(",|\\r\\n");

            int i = 0;
            while(i<movies.length) {
                while (scanner.hasNext()) {

                    String title = scanner.next();
                    int year = scanner.nextInt();
                    int length = scanner.nextInt();
                    String director = scanner.next();

                    movies[i] = new Movie(title, year, length, director);
                    i++;
                }

            }
        } catch (IOException e) {
            System.out.println("Error");
        }
        return movies;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
