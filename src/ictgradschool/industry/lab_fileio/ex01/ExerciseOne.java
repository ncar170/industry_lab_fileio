package ictgradschool.industry.lab_fileio.ex01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {


//        fileReaderEx01();

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }
//
//    private void fileReaderEx01() {
//        int num = 0;
//        FileReader fR = null;
//        try {
//            fR = new FileReader("input1.txt");
//            num = fR.read();
//            System.out.println((char) num);
//            System.out.println((char) fR.read());
//            System.out.println(fR.read());
//            System.out.println(fR.read());
//            System.out.println(fR.read());
//            fR.close();
//        } catch (IOException e) {
//            System.out.println("IO problem");
//        }
//    }


        private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        File file = new File("input2.txt");

        try (FileReader reader = new FileReader(file)) {
            int character = 0;
            while ((character = reader.read()) != -1) {
                total += 1;
                if (((char) character == 'e') || ((char) character == 'E')){
                    numE += 1;
                }
            }
            System.out.println(total);

        } catch (IOException e) {
            System.out.println("IO Error");
        }


        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.



        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        File file = new File("input2.txt");

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            int character = 0;
            String line = null;
            while ((character = reader.read()) != -1) {
                total += 1;
                if (((char) character == 'e') || ((char) character == 'E')) {
                    numE += 1;
                }
            }
            System.out.println(total);

        } catch (IOException e) {
            System.out.println("IO Error");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
